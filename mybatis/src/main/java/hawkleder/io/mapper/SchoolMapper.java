package hawkleder.io.mapper;

import hawkleder.io.pojo.Admin;

import java.util.List;

public interface SchoolMapper {
    /**
     * 获取学校管理员
     * @return
     */
    List<Admin> getSchoolAdmins();

}
