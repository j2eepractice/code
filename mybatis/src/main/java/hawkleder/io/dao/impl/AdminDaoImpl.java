package hawkleder.io.dao.impl;

import hawkleder.io.dao.AdminDao;
import hawkleder.io.pojo.Admin;
import hawkleder.io.utils.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class AdminDaoImpl implements AdminDao {

    @Override
    public Admin getAdmin(Integer id) {

        SqlSession sqlSession =SqlSessionFactoryUtil.openSession();
        Admin admin=sqlSession.selectOne("admin.getAdminByID",id);
        sqlSession.close();
        return admin;
    }

    @Override
    public List<Admin> getAdminByNames(String UserName) {

        SqlSession sqlSession =SqlSessionFactoryUtil.openSession();
        List<Admin> admins=sqlSession.selectList("admin.getAdminByName",UserName);

        sqlSession.close();
        return admins;
    }

    @Override
    public void insertUser(Admin admin) {
        SqlSession sqlSession =SqlSessionFactoryUtil.openSession();
        sqlSession.insert("admin.insertAdmin",admin);
        sqlSession.commit();
        sqlSession.close();
    }
}
