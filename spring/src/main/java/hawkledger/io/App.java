package hawkledger.io;

import hawkledger.io.service.annotation.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
//        ApplicationContext apx =new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
//        System.out.println("容器启动。。。。");

//        UserAction action = (UserAction) apx.getBean("userAction");
//        action.excute();
//        UserAction action2 = (UserAction) apx.getBean("userAction");
//        action2.excute();
//        UserAction action3 = (UserAction) apx.getBean("userAction");
//        action3.excute();
//        ((ClassPathXmlApplicationContext) apx).close();

//        User user=(User) apx.getBean("user2");
//        System.out.println(user);

//        IAdmin adminProxy=new AdminProxy();
//        IAdmin proxy=new ProxyFactoryJDK<IAdmin>(new IAdmin2()).proxy();
//        proxy.save();
//        new ProxyFactoryCGLIB<AdminProxy>(new AdminProxy()).proxy().save();


        ApplicationContext apx =new ClassPathXmlApplicationContext(new String[]{"applicationContextJDBC.xml"});
        UserService userDao=(UserService) apx.getBean("userService");
        userDao.Save();

    }
}
