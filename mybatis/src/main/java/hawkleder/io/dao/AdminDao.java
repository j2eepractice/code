package hawkleder.io.dao;

import hawkleder.io.pojo.Admin;

import java.util.List;


/**
 * 用户信息持久化接口
 *
 */
public interface AdminDao {

    Admin  getAdmin(Integer id);
    List<Admin> getAdminByNames(String UserName);
    void insertUser(Admin admin);

}
