package hawkledger.io.entity;

import java.util.List;
import java.util.Map;

public class User {

    List<User> list;
    Map<String,User> userMap;

    public User(){}
    public User(String name,int age){
        System.out.println("创建bean");
    }

    public List<User> getList() {
        return list;
    }

    public Map<String, User> getUserMap() {
        return userMap;
    }

    public void setList(List<User> list) {
        this.list = list;
    }

    public void setUserMap(Map<String, User> userMap) {
        this.userMap = userMap;
    }
}
