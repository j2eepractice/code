package hawkledger.io.action;

import hawkledger.io.entity.User;
import hawkledger.io.service.annotation.UserService;

//创建多例Action,每次访问都要创建，
public class UserAction {
    public UserService userServices;
    public User user;

    public String excute(){

        return "sucess";
    }

    public void init(){
        System.out.println("init...");
    }

    public void  destory(){
        System.out.println("destory...");
    }

    public UserService getUserServices() {
        return userServices;
    }

    public void setUserServices(UserService userServices) {
        this.userServices = userServices;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
