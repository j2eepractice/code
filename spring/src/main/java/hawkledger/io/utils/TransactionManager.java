package hawkledger.io.utils;
import org.aspectj.lang.ProceedingJoinPoint;

//切面类
public class TransactionManager  {
    public void beginTransaction(){
        System.out.println("开始事务");
    }

    public void commit(){
        System.out.println("事务提交");
    }

    public  void afterReturning(){
        System.out.println("返回后通知");
    }
    public void afterThrowing(){
        System.out.println("异常通知");
    }
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        System.out.println("环绕前");
        proceedingJoinPoint.proceed();//执行目标方法
       String name= proceedingJoinPoint.getTarget().getClass().getName();
        System.out.println("name:"+name);
        System.out.println("环绕后");
    }
}
