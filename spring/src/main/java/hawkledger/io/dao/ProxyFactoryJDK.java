package hawkledger.io.dao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @param <T>
 */
public class ProxyFactoryJDK<T> {

    private T target;

    public ProxyFactoryJDK(T target) {
        this.target = target;
    }

    public Object getProxyInstance() {

        return Proxy.newProxyInstance(
                this.target.getClass().getClassLoader(),
                this.target.getClass().getInterfaces(),
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] params) throws Throwable {

                        Object result = method.invoke(target, params);

                        return result;
                    }

                });
    }

    public Object proxy( ) {

        return this.getProxyInstance();
    }

}
