package hawkleder.io.pojo;

public class Admin {
    private int id;
    private String name;
    private int age;
    private String uuid2;

    public void setUuid2(String uuid2) {
        this.uuid2 = uuid2;
    }

    public String getUuid2() {
        return uuid2;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name="+this.name+" age="+this.age;
    }
}
