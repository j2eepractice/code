package hawkledger.io.service.annotation;

import hawkledger.io.dao.UserDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public  class UserService{
    @Resource
    UserDao userDao;

    @Transactional(  timeout = 10,isolation = Isolation.REPEATABLE_READ ,readOnly = true)
    public void Save(){
        System.out.println("事务代码运行");
    }
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}