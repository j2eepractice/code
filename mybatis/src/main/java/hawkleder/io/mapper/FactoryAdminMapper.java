package hawkleder.io.mapper;

import hawkleder.io.pojo.Admin;

import java.util.List;

public interface FactoryAdminMapper {
    List<Admin> getFactoryAdmins();
}
