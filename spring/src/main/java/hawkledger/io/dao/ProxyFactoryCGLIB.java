package hawkledger.io.dao;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @param <T>
 */
public class ProxyFactoryCGLIB<T> implements MethodInterceptor {

    private T target;
    public ProxyFactoryCGLIB( T target){
        this.target=target;
    }
    public T proxy(){
        Enhancer enhancer=new Enhancer();
        enhancer.setSuperclass(this.target.getClass());//设置要代理的类
        enhancer.setCallback(this);//设置实现MethodInterceptor的类
        return (T) enhancer.create();


    }

    /**
     *
     * @param obj 要增强的对象
     * @param method 拦截的方法
     * @param objects  参数列表
     * @param methodProxy MethodProxy表示对方法的代理，invokeSuper方法表示对被代理对象方法的调用
     * @return
     * @throws Throwable
     */
    public Object intercept(Object obj, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
      //Object result=method.invoke(this.target,objects);
        System.out.println("事🈚关闭");
        methodProxy.invokeSuper(obj,objects);
        System.out.println("事🈚关闭 ");
        return obj;
    }
}
