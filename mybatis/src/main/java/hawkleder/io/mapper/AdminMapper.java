package hawkleder.io.mapper;

import hawkleder.io.pojo.Admin;

import java.util.List;


/**
 * 用户信息持久化接口
 *
 */
public interface AdminMapper {

    Admin  getAdminByID(Integer id);
    List<Admin> getAdminByName(String UserName);
    void insertAdmin(Admin admin);

}
