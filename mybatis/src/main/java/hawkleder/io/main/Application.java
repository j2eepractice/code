package hawkleder.io.main;


import hawkleder.io.dao.AdminDao;
import hawkleder.io.dao.impl.AdminDaoImpl;
import hawkleder.io.mapper.AdminMapper;
import hawkleder.io.mapper.FactoryAdminMapper;
import hawkleder.io.mapper.SchoolMapper;
import hawkleder.io.pojo.Admin;
import hawkleder.io.utils.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Application {

    private static Logger log = Logger.getLogger(Application.class);
    public static void main(String[] args) {
        SqlSession sqlSession = null;
        try {

            sqlSession = SqlSessionFactoryUtil.openSession();
//            //Admin a=sqlSession.selectOne("admin.getAdminByName","xiaoming");
//            List<Admin> a=sqlSession.selectList("admin.getAdminByName","x%");
//            for(Admin admin:a){
//            log.info(admin);
//
//            }

//            for (int i=1;i<100;i++){
//                Admin admin=new Admin();
//                admin.setAge(i);
//                admin.setName("hello");
//                admin.setId(i);
//                log.info(admin);
//                int n=sqlSession.update("admin.deleteName",admin);
//                log.info("outpu n=>"+admin.getId());
//            }
//            sqlSession.commit();

   //  AdminDao adminDao=new AdminDaoImpl();
////            Admin admin=adminDao.getAdmin(1009);
//           List< Admin > admins=adminDao.getAdminByNames("n");
//
//            for(Admin ad:admins){
//                log.info(ad);
//            }
//

//
//            for (int i=1;i<100;i++){
//                Admin admin=new Admin();
//                admin.setAge(i);
//                admin.setName("hello");
//                admin.setId(i);
//                log.info(admin);
//               adminDao.insertUser(admin);
//                log.info("outpu n=>"+admin.getId());
//            }
//
//            SqlSession sqlSession1=SqlSessionFactoryUtil.openSession();
//            AdminMapper m=sqlSession1.getMapper(AdminMapper.class);
//            for (int i=1000;i<10000;i++){
//                Admin admin=new Admin();
//                admin.setAge(i);
//                admin.setName("hello");
//                admin.setId(i);
//                log.info(admin);
//                m.insertAdmin(admin);
//                log.info("outpu n=>"+admin.getId());
//            }
//
//            sqlSession1.commit();
//            sqlSession1.close();

          //  学校管理员mapper
//            SqlSession sqlSession1 = SqlSessionFactoryUtil.openSession();
//            SchoolMapper m = sqlSession1.getMapper(SchoolMapper.class);
//            List<Admin> admins=m.getSchoolAdmins();
//            for(Admin admin:admins){
//                log.info(admin);
//            }


            SqlSession sqlSession1 = SqlSessionFactoryUtil.openSession();
            FactoryAdminMapper m = sqlSession1.getMapper(FactoryAdminMapper.class);
            List<Admin> admins=m.getFactoryAdmins();
            for(Admin admin:admins){
                log.info(admin);
            }


        } catch (Exception e){
           log.info(e);
        }finally {
            if(sqlSession!=null){
                sqlSession.close();
            }
        }
    }
}
